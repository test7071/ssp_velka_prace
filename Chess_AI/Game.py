import pygame
import ChessBoard as B
import BobJr
from multiprocessing import Process, Queue

SCREEN_WIDTH = 400
SCREEN_HEIGHT = 400

def Run(screen, p_one, p_two, difficulty = 0, color_one = "#e8cbb9", color_two = "#c2a390"):

    pygame.init()
    screen.fill(pygame.Color("white"))

    chess = B.ChessBoard()
    valid_moves = chess.get_valid_moves()

    move_made = False
    running = True
    game_over = False

    player_one = p_one
    player_two = p_two
    ai_thinking = False

    chess.load_img()

    selected = ()
    player_clicks = []

    while running:
        human_turn = (chess.white_turn and player_one) or (not chess.white_turn and player_two)
        for event in pygame.event.get():
            if event.type == B.KEYDOWN:
                if event.key == B.K_ESCAPE:
                    running = False      
 
            if event.type == B.MOUSEBUTTONDOWN:
                if not game_over:
                    pos = pygame.mouse.get_pos()
                    c = pos[0]//B.SQ_SIZE
                    r = pos[1]//B.SQ_SIZE

                    if  selected == (r, c) or c >= 8:
                        selected = ()
                        player_clicks = []
                    else:
                        selected = (r, c)
                        player_clicks.append(selected)

                    if len(player_clicks) == 2 and human_turn:
                        move = B.Movement(player_clicks[0], player_clicks[1], chess.board_state, chess.board_positions)
                        for i in range(len(valid_moves)):
                            if move == valid_moves[i]:
                                chess.record_move(valid_moves[i])
                                move_made = True
                                selected = ()
                                player_clicks = []

                        if not move_made:
                            player_clicks = [selected]

            elif event.type == B.QUIT:
                running = False


        if not game_over and not human_turn:
            if not ai_thinking:
                ai_thinking = True
                return_queue = Queue()
                move_finder_process = Process(target=BobJr.find_move, args=(chess, valid_moves, return_queue, difficulty))
                print("Just wait ...")
                move_finder_process.start()

            if not move_finder_process.is_alive():
                ai_move = return_queue.get()
                if ai_move is None:
                    ai_move = BobJr.find_random_move(valid_moves)
                chess.record_move(ai_move)
                move_made = True
                ai_thinking = False       

        if move_made: 
            valid_moves = chess.get_valid_moves()
            move_made = False

        chess.draw_board_state(screen, valid_moves, selected, color_one, color_two)

        if chess.checkmate:
            game_over = True
            if chess.white_turn:
                chess.draw_end_game_text(screen, "Black wins")
            else:
                chess.draw_end_game_text(screen, "White wins")

        elif chess.stalemate:
            game_over = True
            chess.draw_end_game_text(screen, "Stalemate")


        pygame.display.flip()