import pygame.image
import pygame

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500
SQ_SIZE = SCREEN_HEIGHT//8
IMAGES = {}
board = []

class ChessBoard():

    def __init__(self):
        self.white_turn = True
        self.move_functions = {'p' : self.pawn_moves, 'r' : self.rook_moves,
                               'b' : self.bishop_moves, 'n' : self.knight_moves,
                               'q' : self.queen_moves, 'k' : self.king_moves}
        self.board_state = [
            ["Br","Bn","Bb","Bq","Bk","Bb","Bn","Br"],
            ["Bp","Bp","Bp","Bp","Bp","Bp","Bp","Bp"],
            ["..","..","..","..","..","..","..",".."],
            ["..","..","..","..","..","..","..",".."],
            ["..","..","..","..","..","..","..",".."],
            ["..","..","..","..","..","..","..",".."],
            ["Wp","Wp","Wp","Wp","Wp","Wp","Wp","Wp"],
            ["Wr","Wn","Wb","Wq","Wk","Wb","Wn","Wr"]]

        self.board_positions = [
            ["a8","b8","c8","d8","e8","f8","g8","h8"],
            ["a7","b7","c7","d7","e7","f7","g7","h7"],
            ["a6","b6","c6","d6","e6","f6","g6","h6"],
            ["a5","b5","c5","d5","e5","f5","g5","h5"],
            ["a4","b4","c4","d4","e4","f4","g4","h4"],
            ["a3","b3","c3","d3","e3","f3","g3","h3"],
            ["a2","b2","c2","d2","e2","f2","g2","h2"],
            ["a1","b1","c1","d1","e1","f1","g1","h1"]]

        self.w_king_state = (7 , 4)
        self.b_king_state = (0 , 4)
        self.checkmate = False
        self.stalemate = False
        self.in_check = False
        self.pins = []
        self.checks = []
        self.move_log = []
        self.empassant_possible = ()
        self.enpassant_possible_log = [self.empassant_possible]
        self.current_castling_rights = CastleRights(True, True, True, True)
        self.castle_rights_log = [CastleRights(self.current_castling_rights.wks, self.current_castling_rights.bks,
                                               self.current_castling_rights.wqs, self.current_castling_rights.bqs)]

    def load_img(self):
        IMAGES["Bp"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_pawn.png"),(SQ_SIZE, SQ_SIZE)) 
        IMAGES["Br"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_rook.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Bn"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_knight.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Bb"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_bishop.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Bq"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_queen.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Bk"] = pygame.transform.scale(pygame.image.load("ChessPieces/Black_king.png"),(SQ_SIZE, SQ_SIZE))

        IMAGES["Wp"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_pawn.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Wr"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_rook.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Wn"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_knight.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Wb"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_bishop.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Wq"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_queen.png"),(SQ_SIZE, SQ_SIZE))
        IMAGES["Wk"] = pygame.transform.scale(pygame.image.load("ChessPieces/White_king.png"),(SQ_SIZE, SQ_SIZE))

    def draw_board_state(self, screen, valid_moves, square_selected, color_one, color_two):
        self.redraw_board(screen, color_one, color_two)
        self.highlight_squares(screen, valid_moves, square_selected)
        self.redraw_rooks(screen, self.board_state)

    def redraw_board(self, screen, color_one, color_two):
        colors = [pygame.Color(color_one), pygame.Color(color_two)]
        for r in range(8):
            for c in range(8):
                color = colors[((r+c) % 2)]
                pygame.draw.rect(screen, color, pygame.Rect(c * SQ_SIZE, r * SQ_SIZE, SQ_SIZE, SQ_SIZE))
    
    def redraw_rooks(self, screen, board):
        for r in range(8):
            for c in range(8):
                piece = board[r][c]
                if piece != "..":
                    screen.blit(IMAGES[piece], pygame.Rect(c * SQ_SIZE, r * SQ_SIZE, SQ_SIZE, SQ_SIZE))

    def highlight_squares(self, screen, valid_moves, square_selected):
        if square_selected != ():
            row, col = square_selected
            if self.board_state[row][col][0] == ('W' if self.white_turn else 'B'):  
                s = pygame.Surface((SQ_SIZE, SQ_SIZE))
                s.set_alpha(100) 
                s.fill(pygame.Color('blue'))
                screen.blit(s, (col * SQ_SIZE, row * SQ_SIZE))
                s.fill(pygame.Color('yellow'))
                for move in valid_moves:
                    if move.begin_r == row and move.begin_c == col:
                        screen.blit(s, (move.end_c * SQ_SIZE, move.end_r * SQ_SIZE))


    def draw_end_game_text(self, screen, text):
        font = pygame.font.SysFont("Helvetica", 32, True, False)
        text_object = font.render(text, False, pygame.Color("gray"))
        text_location = pygame.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT).move(SCREEN_WIDTH / 2 - text_object.get_width() / 2,
                                                                 SCREEN_HEIGHT / 2 - text_object.get_height() / 2)
        screen.blit(text_object, text_location)
        text_object = font.render(text, False, pygame.Color('black'))
        screen.blit(text_object, text_location.move(2, 2))

    def inCheck(self):
        if self.white_turn:
            return self.square_under_attack(self.w_king_state[0], self.w_king_state[1])
        else:
            return self.square_under_attack(self.b_king_state[0], self.b_king_state[1])

    def square_under_attack(self, row, col):
        self.white_turn = not self.white_turn  
        opponents_moves = self.get_all_possible_moves()
        self.white_turn = not self.white_turn
        for move in opponents_moves:
            if move.end_r == row and move.end_c == col:
                return True
        return False
    
    def record_move(self, move):
        self.board_state[move.begin_r][move.begin_c] = ".."
        self.board_state[move.end_r][move.end_c] = move.piece_moved
        self.white_turn = not self.white_turn 
        self.move_log.append(move)

        if move.piece_moved == "Wk":
            self.w_king_state = (move.end_r, move.end_c)
        elif move.piece_moved == "Bk":
            self.b_king_state = (move.end_r, move.end_c)

        if move.is_pawn_promotion:
            self.board_state[move.end_r][move.end_c] = move.piece_moved[0] + 'q'
        
        if move.is_empassant_move:
            self.board_state[move.begin_r][move.end_c] = ".."
        
        if move.piece_moved[1] == "p" and abs(move.begin_r - move.end_r) == 2: 
            self.empassant_possible = ((move.begin_r + move.end_r) // 2, move.begin_c)
        else:
            self.empassant_possible = ()

        if move.is_castle_move:
            if move.end_c - move.begin_c == 2: 
                self.board_state[move.end_r][move.end_c - 1] = self.board_state[move.end_r][
                    move.end_c + 1]  
                self.board_state[move.end_r][move.end_c + 1] = '..'
            else: 
                self.board_state[move.end_r][move.end_c + 1] = self.board_state[move.end_r][
                    move.end_c - 2] 
                self.board_state[move.end_r][move.end_c - 2] = '..'

        self.enpassant_possible_log.append(self.empassant_possible)

        self.update_castle(move)
        self.castle_rights_log.append(CastleRights(self.current_castling_rights.wks, self.current_castling_rights.bks,
                                                   self.current_castling_rights.wqs, self.current_castling_rights.bqs))

    
    def undo_move(self):
        if len(self.move_log) != 0:
            move = self.move_log.pop()
            self.board_state[move.begin_r][move.begin_c] = move.piece_moved
            self.board_state[move.end_r][move.end_c] = move.piece_destroyed
            self.white_turn = not self.white_turn  

            if move.piece_moved == "Wk":
                self.white_king_location = (move.begin_r, move.begin_c)
            elif move.piece_moved == "Bk":
                self.black_king_location = (move.begin_r, move.begin_c)

            if move.is_empassant_move:
                self.board_state[move.end_r][move.end_c] = ".." 
                self.board_state[move.begin_r][move.end_c] = move.piece_destroyed

            self.enpassant_possible_log.pop()
            self.empassant_possible = self.enpassant_possible_log[-1]

            self.castle_rights_log.pop()  
            self.current_castling_rights = self.castle_rights_log[-1]  

            if move.is_castle_move:
                if move.end_c - move.begin_c == 2: 
                    self.board_state[move.end_r][move.end_c + 1] = self.board_state[move.end_r][move.end_c - 1]
                    self.board_state[move.end_r][move.end_c - 1] = '..'
                else:  
                    self.board_state[move.end_r][move.end_c - 2] = self.board_state[move.end_r][move.end_c + 1]
                    self.board_state[move.end_r][move.end_c + 1] = '..'

            self.checkmate = False
            self.stalemate = False


    def get_valid_moves(self):
        temp_castle_rights = CastleRights(self.current_castling_rights.wks, self.current_castling_rights.bks,
                                          self.current_castling_rights.wqs, self.current_castling_rights.bqs)

        moves = []
        self.in_check, self.pins, self.checks = self.check_for_pins_and_checks()

        if self.white_turn:
            king_row = self.w_king_state[0]
            king_col = self.w_king_state[1]
        else:
            king_row = self.b_king_state[0]
            king_col = self.b_king_state[1]
        if self.in_check:
            if len(self.checks) == 1:
                moves = self.get_all_possible_moves()
                check = self.checks[0]
                check_row = check[0]
                check_col = check[1]
                piece_checking = self.board_state[check_row][check_col]
                valid_squares = [] 
                if piece_checking[1] == "n":
                    valid_squares = [(check_row, check_col)]
                else:
                    for i in range(1, 8):
                        valid_square = (king_row + check[2] * i, king_col + check[3] * i)
                        valid_squares.append(valid_square)
                        if valid_square[0] == check_row and valid_square[1] == check_col:
                            break
                for i in range(len(moves) - 1, - 1, -1): 
                    if moves[i].piece_moved[1] != "k":
                        if not (moves[i].end_r, moves[i].end_c) in valid_squares:
                            moves.remove(moves[i])
            else:
                self.king_moves(king_row, king_col, moves)
        else:
            moves = self.get_all_possible_moves()
            if self.white_turn:
                self.get_castle_moves(self.w_king_state[0], self.w_king_state[1], moves)
            else:
                self.get_castle_moves(self.b_king_state[0], self.b_king_state[1], moves)
        if len(moves) == 0:
            if self.inCheck():
                self.checkmate = True
            else:
                self.stalemate = True
        else:
            self.checkmate = False
            self.stalemate = False

        self.current_castling_rights = temp_castle_rights

        return moves

    def update_castle(self, move):
        if move.piece_destroyed == "Wr":
            if move.end_c == 0: 
                self.current_castling_rights.wqs = False
            elif move.end_c == 7: 
                self.current_castling_rights.wks = False
        elif move.piece_destroyed == "Br":
            if move.end_c == 0:  
                self.current_castling_rights.bqs = False
            elif move.end_c == 7: 
                self.current_castling_rights.bks = False

        if move.piece_moved == 'Wk':
            self.current_castling_rights.wqs = False
            self.current_castling_rights.wks = False
        elif move.piece_moved == 'Bk':
            self.current_castling_rights.bqs = False
            self.current_castling_rights.bks = False
        elif move.piece_moved == 'Wr':
            if move.begin_r == 7:
                if move.begin_c == 0: 
                    self.current_castling_rights.wqs = False
                elif move.begin_c == 7:  
                    self.current_castling_rights.wks = False
        elif move.piece_moved == 'Br':
            if move.begin_r == 0:
                if move.begin_c == 0: 
                    self.current_castling_rights.bqs = False
                elif move.begin_c == 7: 
                    self.current_castling_rights.bks = False

    def check_for_pins_and_checks(self):
        pins = []
        checks = []
        in_check = False
        if self.white_turn:
            enemy_color = "B"
            ally_color = "W"
            start_row = self.w_king_state[0]
            start_col = self.w_king_state[1]
        else:
            enemy_color = "W"
            ally_color = "B"
            start_row = self.b_king_state[0]
            start_col = self.b_king_state[1]
        directions = ((-1, 0), (0, -1), (1, 0), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1))
        for j in range(len(directions)):
            d = directions[j]
            possible_pin = ()
            for i in range(1, 8):
                end_row = start_row + d[0] * i
                end_col = start_col + d[1] * i
                if 0 <= end_row < 8 and 0 <= end_col < 8:
                    end_piece = self.board_state[end_row][end_col]
                    if end_piece[0] == ally_color and end_piece[1] != 'k':
                        if possible_pin == ():
                            possible_pin = (end_row, end_col, d[0], d[1])
                        else:
                            break
                    elif end_piece[0] == enemy_color:
                        type = end_piece[1]
                        if (0 <= j <= 3 and type == "r") or (4 <= j <= 7 and type == "b") or\
                                (i == 1 and type == "p" and ((enemy_color == 'W' and 6 <= j <= 7) or (enemy_color == 'B' and 4 <= j <= 5))) or \
                                (type == 'q') or (i == 1 and type == 'k'):
                            if possible_pin == ():
                                in_check = True
                                checks.append((end_row, end_col, d[0], d[1]))
                                break
                            else:
                                pins.append(possible_pin)
                                break
                        else:
                            break
                else:
                    break
        knight_moves = ((-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1))
        for m in knight_moves:
            end_row = start_row + m[0]
            end_col = start_col + m[1]
            if 0 <= end_row < 8 and 0 <= end_col < 8:
                end_piece = self.board_state[end_row][end_col]
                if end_piece[0] == enemy_color and end_piece[1] == 'n':
                    in_check = True
                    checks.append((end_row, end_col, m[0], m[1]))

        return in_check, pins, checks


    def get_all_possible_moves(self):
        moves = []
        for r in range(len(self.board_state)):
            for c in range(len(self.board_state[r])):
                turn = self.board_state[r][c][0]
                if (turn == "W" and self.white_turn) or (turn == "B" and not self.white_turn):
                    piece = self.board_state[r][c][1]
                    self.move_functions[piece](r, c, moves)
        return moves

    def pawn_moves(self, r, c, moves):
        piece_pinned = False
        pin_direction = ()
        for i in range(len(self.pins) - 1, -1, -1):
            if self.pins[i][0] == r and self.pins[i][1] == c:
                piece_pinned = True
                pin_direction = (self.pins[i][2], self.pins[i][3])
                self.pins.remove(self.pins[i])
                break

        if self.white_turn:
            move_amount = -1
            start_row = 6
            enemy_color = "B"
            king_row, king_col = self.w_king_state
        else:
            move_amount = 1
            start_row = 1
            enemy_color = "W"
            king_row, king_col = self.b_king_state

        if self.board_state[r + move_amount][c] == "..":
            if not piece_pinned or pin_direction == (move_amount, 0):
                moves.append(Movement((r, c), (r + move_amount, c), self.board_state, self.board_positions))
                if r == start_row and self.board_state[r + 2 * move_amount][c] == "..":
                    moves.append(Movement((r, c), (r + 2 * move_amount, c), self.board_state, self.board_positions))
        if c - 1 >= 0:
            if not piece_pinned or pin_direction == (move_amount, -1):
                if self.board_state[r + move_amount][c - 1][0] == enemy_color:
                    moves.append(Movement((r, c), (r + move_amount, c - 1), self.board_state, self.board_positions))

                if (r + move_amount, c - 1) == self.empassant_possible:
                    attacking_piece = blocking_piece = False
                    if king_row == r:
                        if king_col < c:  
                            inside_range = range(king_col + 1, c - 1)
                            outside_range = range(c + 1, 8)
                        else: 
                            inside_range = range(king_col - 1, c, -1)
                            outside_range = range(c - 2, -1, -1)

                        for i in inside_range:
                            if self.board_state[r][i] != "..":
                                blocking_piece = True

                        for i in outside_range:
                            square = self.board_state[r][i]
                            if square[0] == enemy_color and (square[1] == "r" or square[1] == "q"):
                                attacking_piece = True
                            elif square != "..":
                                blocking_piece = True

                    if not attacking_piece or blocking_piece:
                        moves.append(Movement((r, c), (r + move_amount, c - 1), self.board_state, self.board_positions, empassant_possible=True))
        if c + 1 <= 7:
            if not piece_pinned or pin_direction == (move_amount, +1):
                if self.board_state[r + move_amount][c + 1][0] == enemy_color:
                    moves.append(Movement((r, c), (r + move_amount, c + 1), self.board_state, self.board_positions))

                if (r + move_amount, c + 1) == self.empassant_possible:
                    attacking_piece = blocking_piece = False
                    if king_row == r:
                        if king_col < c: 
                            inside_range = range(king_col + 1, c)
                            outside_range = range(c + 2, 8)
                        else:  
                            inside_range = range(king_col - 1, c + 1, -1)
                            outside_range = range(c - 1, -1, -1)

                        for i in inside_range:
                            if self.board_state[r][i] != "..":
                                blocking_piece = True

                        for i in outside_range:
                            square = self.board_state[r][i]
                            if square[0] == enemy_color and (square[1] == "r" or square[1] == "q"):
                                attacking_piece = True
                            elif square != "..":
                                blocking_piece = True

                    if not attacking_piece or blocking_piece:
                        moves.append(Movement((r, c), (r + move_amount, c + 1), self.board_state, self.board_positions, empassant_possible=True))


    def rook_moves(self, r, c, moves):
        piece_pinned = False
        pin_direction = ()
        for i in range(len(self.pins) - 1, -1, -1):
            if self.pins[i][0] == r and self.pins[i][1] == c:
                piece_pinned = True
                pin_direction = (self.pins[i][2], self.pins[i][3])
                if self.board_state[r][c][1] != 'q':
                    self.pins.remove(self.pins[i])
                break

        directions = ((-1, 0), (0, -1), (1, 0), (0, 1))
        enemy_color = "B" if self.white_turn else "W"
        for d in directions:
            for i in range(1, 8):
                endRow = r + d[0] * i
                endCol = c + d[1] * i
                if 0 <= endRow <= 7 and 0 <= endCol <= 7:
                    if not piece_pinned or pin_direction == d or pin_direction == (-d[0] , -d[1]):
                        end_piece = self.board_state[endRow][endCol]
                        if end_piece == "..":
                            moves.append(Movement((r , c),(endRow, endCol), self.board_state, self.board_positions))
                        elif end_piece[0] == enemy_color:
                            moves.append(Movement((r, c), (endRow, endCol), self.board_state, self.board_positions))
                            break  
                        else:
                            break
                else:
                    break

    def knight_moves(self, r, c, moves):
        piece_pinned = False
        for i in range(len(self.pins) - 1, -1, -1):
            if self.pins[i][0] == r and self.pins[i][1] == c:
                piece_pinned = True
                self.pins.remove(self.pins[i])
                break

        directions = ((-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1)) 
        enemy_color = "W" if self.white_turn else "B"
        for d in directions:
            endRow = r + d[0]
            endCol = c + d[1]
            if 0 <= endRow <= 7 and 0 <= endCol <= 7:
                if not piece_pinned:
                    end_piece = self.board_state[endRow][endCol]
                    if end_piece[0] != enemy_color:
                        moves.append(Movement((r , c),(endRow, endCol), self.board_state, self.board_positions))


    def bishop_moves(self, r, c, moves):
        piece_pinned = False
        pin_direction = ()
        for i in range(len(self.pins) - 1, -1, -1):
            if self.pins[i][0] == r and self.pins[i][1] == c:
                piece_pinned = True
                pin_direction = (self.pins[i][2], self.pins[i][3])
                self.pins.remove(self.pins[i])
                break

        directions = ((-1, -1), (-1, 1), (1, -1), (1, 1)) 
        enemy_color = "B" if self.white_turn else "W"
        for d in directions:
            for i in range(1, 8):
                endRow = r + d[0] * i
                endCol = c + d[1] * i
                if 0 <= endRow <= 7 and 0 <= endCol <= 7:
                    if not piece_pinned or pin_direction == d or pin_direction == (-d[0] , -d[1]):
                        end_piece = self.board_state[endRow][endCol]
                        if end_piece == "..":
                            moves.append(Movement((r , c),(endRow, endCol), self.board_state, self.board_positions))
                        elif end_piece[0] == enemy_color:
                            moves.append(Movement((r , c),(endRow, endCol), self.board_state, self.board_positions))
                            break
                        else:
                            break
                else:
                    break

    def queen_moves(self, r, c, moves):
       self.rook_moves(r, c, moves)
       self.bishop_moves(r, c, moves)

    def king_moves(self, r, c, moves):
        row_moves = (-1, -1, -1, 0, 0, 1, 1, 1)
        col_moves = (-1, 0, 1, -1, 1, -1, 0, 1)  
        enemy_color = "W" if self.white_turn else "B"
        for i in range(8):
            endRow = r + row_moves[i]
            endCol = c + col_moves[i]
            if 0 <= endRow <= 7 and 0 <= endCol <= 7:
                end_piece = self.board_state[endRow][endCol]
                if end_piece[0] != enemy_color:
                    if enemy_color == "W":
                        self.w_king_state = (endRow, endCol)
                    else:
                        self.b_king_state = (endRow, endCol)
                    in_check, pins, checks = self.check_for_pins_and_checks()
                    if not in_check:
                        moves.append(Movement((r , c),(endRow, endCol), self.board_state, self.board_positions))
                    if enemy_color == "W":
                        self.w_king_state = (r, c)
                    else:
                        self.b_king_state = (r, c)

    def get_castle_moves(self, r, c, moves):
        if self.square_under_attack(r, c):
            return 
        if (self.white_turn and self.current_castling_rights.wks) or (
                not self.white_turn and self.current_castling_rights.bks):
            self.king_castle_moves(r, c, moves)
        if (self.white_turn and self.current_castling_rights.wqs) or (
                not self.white_turn and self.current_castling_rights.bqs):
            self.queen_castle_moves(r, c, moves)

    def king_castle_moves(self, r, c, moves):
        if self.board_state[r][c + 1] == '..' and self.board_state[r][c + 2] == '..':
            if not self.square_under_attack(r, c + 1) and not self.square_under_attack(r, c + 2):
                moves.append(Movement((r, c), (r, c + 2), self.board_state, self.board_positions, is_castle_move=True))

    def queen_castle_moves(self, r, c, moves):
        if self.board_state[r][c - 1] == '..' and self.board_state[r][c - 2] == '..' and self.board_state[r][c - 3] == '..':
            if not self.square_under_attack(r, c - 1) and not self.square_under_attack(r, c - 2):
                moves.append(Movement((r, c), (r, c - 2), self.board_state, self.board_positions, is_castle_move=True))

class CastleRights:
    def __init__(self, wks, bks, wqs, bqs):
        self.wks = wks
        self.bks = bks
        self.wqs = wqs
        self.bqs = bqs


class Movement():
    def __init__(self, begin, end, board, pos, empassant_possible = False, is_castle_move=False):
        self.begin_r = begin[0]
        self.begin_c = begin[1]
        self.end_r = end[0]
        self.end_c = end[1]
        self.pos = pos
        self.piece_moved = board[self.begin_r][self.begin_c]
        self.piece_destroyed = board[self.end_r][self.end_c]

        self.is_pawn_promotion = (self.piece_moved == "Wp" and self.end_r == 0) or (self.piece_moved == "Bp" and self.end_r == 7)

        self.is_empassant_move = empassant_possible

        if self.is_empassant_move:
            self.piece_destroyed = "Wp" if self.piece_moved == "Bp" else "Bp"
        self.is_castle_move = is_castle_move

        self.is_capture = self.piece_destroyed != ".."

        self.moveID = self.begin_r * 1000 + self.begin_c * 100 + self.end_r * 10 + self.end_c

    def __eq__(self, other):
        if isinstance(other, Movement):
            return self.moveID == other.moveID
        return False 

    def print_move(self):
        start = self.pos[self.begin_r][self.begin_c]
        end = self.pos[self.end_r][self.end_c]

        print(start + " -> " +end) 
