import random

piece_score = {"k": 0, "q": 9, "r": 5, "b": 3, "n": 3, "p": 1}

knight_scores = [[0.0, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.0],
                 [0.1, 0.3, 0.5, 0.5, 0.5, 0.5, 0.3, 0.1],
                 [0.2, 0.5, 0.6, 0.65, 0.65, 0.6, 0.5, 0.2],
                 [0.2, 0.55, 0.65, 0.7, 0.7, 0.65, 0.55, 0.2],
                 [0.2, 0.5, 0.65, 0.7, 0.7, 0.65, 0.5, 0.2],
                 [0.2, 0.55, 0.6, 0.65, 0.65, 0.6, 0.55, 0.2],
                 [0.1, 0.3, 0.5, 0.55, 0.55, 0.5, 0.3, 0.1],
                 [0.0, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.0]]

bishop_scores = [[0.0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.0],
                 [0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.2],
                 [0.2, 0.4, 0.5, 0.6, 0.6, 0.5, 0.4, 0.2],
                 [0.2, 0.5, 0.5, 0.6, 0.6, 0.5, 0.5, 0.2],
                 [0.2, 0.4, 0.6, 0.6, 0.6, 0.6, 0.4, 0.2],
                 [0.2, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.2],
                 [0.2, 0.5, 0.4, 0.4, 0.4, 0.4, 0.5, 0.2],
                 [0.0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.0]]

rook_scores = [[0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25],
               [0.5, 0.75, 0.75, 0.75, 0.75, 0.75, 0.75, 0.5],
               [0.0, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.0],
               [0.0, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.0],
               [0.0, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.0],
               [0.0, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.0],
               [0.0, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.0],
               [0.25, 0.25, 0.25, 0.5, 0.5, 0.25, 0.25, 0.25]]

queen_scores = [[0.0, 0.2, 0.2, 0.3, 0.3, 0.2, 0.2, 0.0],
                [0.2, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.2],
                [0.2, 0.4, 0.5, 0.5, 0.5, 0.5, 0.4, 0.2],
                [0.3, 0.4, 0.5, 0.5, 0.5, 0.5, 0.4, 0.3],
                [0.4, 0.4, 0.5, 0.5, 0.5, 0.5, 0.4, 0.3],
                [0.2, 0.5, 0.5, 0.5, 0.5, 0.5, 0.4, 0.2],
                [0.2, 0.4, 0.5, 0.4, 0.4, 0.4, 0.4, 0.2],
                [0.0, 0.2, 0.2, 0.3, 0.3, 0.2, 0.2, 0.0]]

pawn_scores = [[ 0.8,  0.8,  0.8,  0.8,  0.8,  0.8,  0.8,  0.8],
               [ 0.7,  0.7,  0.7,  0.7,  0.7,  0.7,  0.7,  0.7],
               [ 0.3,  0.3,  0.4,  0.5,  0.5,  0.4,  0.3,  0.3],
               [0.25, 0.25,  0.3, 0.45, 0.45,  0.3, 0.25, 0.25],
               [ 0.2,  0.2,  0.2,  0.4,  0.4,  0.2,  0.2,  0.2],
               [0.25, 0.15,  0.1,  0.2,  0.2,  0.1, 0.15, 0.25],
               [0.25,  0.3,  0.3,  0.0,  0.0,  0.3,  0.3, 0.25],
               [ 0.2,  0.2,  0.2,  0.2,  0.2,  0.2,  0.2,  0.2]]

piece_position_scores = {"Wn": knight_scores,
                         "Bn": knight_scores[::-1],
                         "Wb": bishop_scores,
                         "Bb": bishop_scores[::-1],
                         "Wq": queen_scores,
                         "Bq": queen_scores[::-1],
                         "Wr": rook_scores,
                         "Br": rook_scores[::-1],
                         "Wp": pawn_scores,
                         "Bp": pawn_scores[::-1]}

CHECKMATE = 1000
STALEMATE = 0


def find_move(board, valid_moves, return_queue, difficulty):
    global next_move
    DEPTH = difficulty
    next_move = None

    random.shuffle(valid_moves)

    move_max_alpha_beta(board, valid_moves, DEPTH, -CHECKMATE, CHECKMATE, 1 if board.white_turn else -1, difficulty)

    return_queue.put(next_move)

def ScoreBoard(board):
    if board.checkmate:
        if board.white_turn:
            return -CHECKMATE  
        else:
            return CHECKMATE 

    elif board.stalemate:
        return STALEMATE

    score = 0

    for row in range(len(board.board_state)):
        for col in range(len(board.board_state[row])):
            piece = board.board_state[row][col]
            if piece != "..":
                piece_position_score = 0
                if piece[1] != "k":
                    piece_position_score = piece_position_scores[piece][row][col]
                if piece[0] == "W":
                    score += piece_score[piece[1]] + piece_position_score
                if piece[0] == "B":
                    score -= piece_score[piece[1]] + piece_position_score

    return score

def move_max_alpha_beta(board, valid_moves, depth, alpha, beta, turn_multiplier, DEPTH):
    global next_move

    if depth == 0:
        return turn_multiplier * ScoreBoard(board)

    max_score = -CHECKMATE

    for move in valid_moves:
        board.record_move(move)
        next_moves = board.get_valid_moves()
        score = -move_max_alpha_beta(board, next_moves, depth - 1, -beta, -alpha, -turn_multiplier, DEPTH)

        if score > max_score:
            max_score = score
            if depth == DEPTH:
                next_move = move

        board.undo_move()

        if max_score > alpha:
            alpha = max_score
        if alpha >= beta:
            break

    return max_score


def find_random_move(valid_moves):
    return random.choice(valid_moves)