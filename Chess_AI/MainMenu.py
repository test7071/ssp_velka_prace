import pygame, sys
import ChessBoard as B
import Game as game

from pygame.locals import *
 
BUTTON_HEIGHT = 50
BUTTON_WIDTH = 220

class MainMenu:
    def __init__(self):
        self.color_one = "#e8cbb9"
        self.color_two = "#c2a390"

    def draw_text(self, text, font, color, surface, x, y):

        textobj = font.render(text, 1, color)
        textrect = textobj.get_rect()
        textrect.topleft = (x, y)
        surface.blit(textobj, textrect)

    def options(self,screen, font):
        running = True
        text = ["wood", "classic", "eye destroyer"]
        while running:
            screen.fill("#485263")
 
            self.draw_text('Change board colour', font, (255, 255, 255), screen, 20, 20)

            buttons = self.draw_buttons(screen, font, 3, text)
            mx, my = pygame.mouse.get_pos()

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if buttons[0].collidepoint((mx, my)):
                        self.color_one = "#e8cbb9"
                        self.color_two = "#c2a390"
                        running = False

                    if buttons[1].collidepoint((mx, my)):
                        self.color_one = "white"
                        self.color_two = "grey"
                        running = False
                    
                    if buttons[2].collidepoint((mx, my)):
                        self.color_one = "blue"
                        self.color_two = "red"
                        running = False
                        

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        running = False
        
            pygame.display.update()

    def draw_buttons(self, screen, font, number, texts):
            buttons = []
            move = 0
            num = 10
            spacing = 70

            for b in range(0, number):
                buttons.append(pygame.Rect(B.SCREEN_HEIGHT/2 - BUTTON_WIDTH/2, B.SCREEN_HEIGHT/2 - BUTTON_HEIGHT + move, BUTTON_WIDTH, BUTTON_HEIGHT))
                pygame.draw.rect(screen, "#7ec0ee", buttons[b])
                move += spacing

            for text in texts:
                self.draw_text(text, font, (255, 255, 255), screen, B.SCREEN_WIDTH/2 - len(text) * 8, B.SCREEN_HEIGHT/2 - BUTTON_HEIGHT + num)
                num += spacing

            return buttons

    def game_settings(self, screen, font):
        running = True
        text = ["P vs P", "P vs Bob"]
        setting_buttons = []

        while running:
            screen.fill("#485263")
 
            self.draw_text('Game mode', font, (255, 255, 255), screen, B.SCREEN_WIDTH/2 - 80, B.SCREEN_HEIGHT/5)

            setting_buttons = self.draw_buttons(screen, font, 2, text)

            mx, my = pygame.mouse.get_pos()

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if setting_buttons[0].collidepoint((mx, my)):
                        game.Run(screen, True, True, 0, self.color_one, self.color_two)

                    if setting_buttons[1].collidepoint((mx, my)):
                        self.game_difficulty(screen, font)

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        running = False
        
            pygame.display.update()

    def game_difficulty(self, screen, font):
        running = True
        text = ["hard", "normal", "easy"]
        buttons = []

        while running:
            screen.fill("#485263")
            mx, my = pygame.mouse.get_pos()
 
            self.draw_text('Game difficulty', font, (255, 255, 255), screen, B.SCREEN_WIDTH/2 - 100, B.SCREEN_HEIGHT/5)

            buttons = self.draw_buttons(screen, font, 3, text)

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if buttons[0].collidepoint((mx, my)):
                        game.Run(screen, True, False, 3, self.color_one, self.color_two)

                    if buttons[1].collidepoint((mx, my)):
                        game.Run(screen, True, False, 2, self.color_one, self.color_two)

                    if buttons[2].collidepoint((mx, my)):
                        game.Run(screen, True, False, 1, self.color_one, self.color_two)

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        running = False
        
            pygame.display.update()

 
    def Run(self):
        pygame.init()
        screen = pygame.display.set_mode((B.SCREEN_WIDTH, B.SCREEN_HEIGHT))
        text = ["play", "options"]
        buttons = []

        while True:
            font = pygame.font.SysFont(None, 40)
            screen.fill("#485263")
            self.draw_text('main menu', font, (255, 255, 255), screen, B.SCREEN_WIDTH/2 - 80, B.SCREEN_HEIGHT/5)
 
            mx, my = pygame.mouse.get_pos()

            buttons = self.draw_buttons(screen, font, 2, text)

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if buttons[0].collidepoint((mx, my)):
                        self.game_settings(screen, font)

                    if buttons[1].collidepoint((mx, my)):
                        self.options(screen, font)

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                        pygame.quit()
                        sys.exit()
                
 
            pygame.display.update()