# SSP_velka_prace
Jedná se o chess game vytvořenou v [Python](https://www.python.org/) pomocí pygame s možností nastavit si barvu plochy a styl hry 
- [ ] P v P
- [ ] P v Bob
    - [ ] Hard
    - [ ] Normal
    - [ ] Easy
## Instalace
Pro běh hry je důležité mít stáhnuté všechny potřebné instalace v [required_trojan.txt](https://gitlab.com/test7071/ssp_velka_prace/-/blob/main/required_trojan.txt)
```
# Pygame package
customtkinter==3.8
darkdetect==0.5.1
package-name==0.1
Pillow==9.0.1
pygame==2.1.2
pyglet==1.5.22
pylint==2.13.5
```
## O hře
Navigace zpátky pomocí **ESC**
Na main je možnost hned jít do volby hry nebo si změnit barvu šachovnice <br /> 
<p float="Center">
  <img src="./Screenshot/main.png" width="350">
  <img src="./Screenshot/colour.png" width="350">
</p>
<br />
V options se dá vybrat ze 3 barvev <br />
1. Wood <br />
2. Classic <br />
3. Eye_destroyer <br />
<br />
<p float="left">
  <img src="./Screenshot/wood.png" width="350" text="wood">
  <img src="./Screenshot/classic.png" width="350">
  <img src="./Screenshot/eye_destroyer.png" width="350">
</p>
<br />
Potom tu je možnost volby stylu hry na výběr tu je proti dalšímu hráčí nebo proti Bob. <br />
Pro Ai Bob jde nastavit i jeho difficulty. <br /> 
<br /> 
<p float="left">
  <img src="./Screenshot/game_mode.png" width="350">
  <img src="./Screenshot/game_difficulty.png" width="350">
</p>
<br />
**Hra podporuje Rochade i Castling**

## O Bob Ai
Ai od Bob je napsané za pomocí algoritmu [Alpha–beta pruning](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning) v code je každé figurce a jakékoliv pozici na šachovnici daná hodnota a pomocí zmíněného algoritmu se rozhoduje další tah.
<br /> 
![Alpha–beta pruning](https://upload.wikimedia.org/wikipedia/en/7/79/Minmaxab.gif)
